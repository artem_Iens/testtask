package ru.amdev.testtaskforcode.interfaces;

import java.io.Serializable;

public interface GeoObject extends Serializable{
    String getTitle();
    int getId();
}
