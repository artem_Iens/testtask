package ru.amdev.testtaskforcode.loaders;

import android.content.Context;
import android.util.Log;

import ru.amdev.testtaskforcode.network.NetworkError;
import ru.amdev.testtaskforcode.network.requests.Request;

public class RequestLoader<T> extends BaseLoader<T> {
    protected Request<T> mRequest;
    protected NetworkError mError;

    public RequestLoader(Context context) {
        super(context);
    }

    public void forceLoad(Request<T> request) {
        mRequest = request;
        forceLoad();
    }

    @Override
    public void forceLoad() {
        super.forceLoad();
        if(mRequest == null) return;
        mRequest.execute(new Request.Callback<T>() {
            @Override
            protected void onCompleted(T result, NetworkError error) {
                mError = error;
                deliverResult(result);
            }
        });
    }

    @Override
    protected void onReset() {
        if(mRequest != null) mRequest.removeCallback();
    }

    public NetworkError getError() {
        return mError;
    }
}
