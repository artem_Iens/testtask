package ru.amdev.testtaskforcode.models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.amdev.testtaskforcode.Constants;
import ru.amdev.testtaskforcode.interfaces.GeoObject;

public class Country implements GeoObject {
    public static final String LOG_TAG = Constants.MY_LOGS + Country.class.getSimpleName();
    public int id;
    public String title;

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public int getId() {
        return id;
    }

    public static ArrayList<Country> createFromJSONArray(JSONArray jsonArray) {
        ArrayList<Country> models = new ArrayList<>();
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                Country model = createFromJSON(jsonArray.getJSONObject(i));
                if (model != null) models.add(model);
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "createFromJSONArray ex: " + e.toString());
            e.printStackTrace();
        }
        return models;
    }

    public static Country createFromJSON(JSONObject jsonObject) {
        Country model = new Country();
        try {
            model.id = Integer.valueOf(jsonObject.getString("cid"));
            model.title = jsonObject.getString("title");
        } catch (JSONException e) {
            Log.e(LOG_TAG, "createFromJSON ex: " + e.toString());
            e.printStackTrace();
            return null;
        }
        return model;
    }
}
