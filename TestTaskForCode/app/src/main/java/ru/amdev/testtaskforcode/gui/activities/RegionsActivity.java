package ru.amdev.testtaskforcode.gui.activities;

import android.content.Intent;
import android.os.Bundle;

import ru.amdev.testtaskforcode.interfaces.GeoObject;
import ru.amdev.testtaskforcode.network.requests.GeoObjectsRequest;
import ru.amdev.testtaskforcode.network.requests.RegionsRequest;

public class RegionsActivity extends DetailGeoObjectsActivity {
    private GeoObject country;

    @Override
    protected void handleIntentExtras() {
        country = (GeoObject) getIntent().getSerializableExtra("country");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(country.getTitle());
    }
    
    @Override
    protected GeoObjectsRequest createRequest(int offset) {
        RegionsRequest request = new RegionsRequest(this)
                .countryId(country.getId());
        request.count(COUNT_OF_MODELS_FOR_ONE_LOADING)
                .offset(offset);
        return request;
    }

    @Override
    public void onItemClick(GeoObject geoObject) {
        Intent intent = new Intent(this, CitiesActivity.class);
        intent.putExtra("region", geoObject);
        intent.putExtra("country", country);
        startActivity(intent);
    }
}
