package ru.amdev.testtaskforcode.network.requests;

import android.content.Context;

import ru.amdev.testtaskforcode.network.MyHttpClient;
import ru.amdev.testtaskforcode.network.NetworkError;

public abstract class Request<T> {
    protected Context mContext;
    protected Callback<T> mCallback;
    protected NetworkError mNetworkError;

    //Do not pass Activity or Fragment to constructors, it may be cause of memory leaks
    //Use Application context instead
    public Request(Context context) {
        mContext = context;
    }

    public void execute(Callback<T> callback) {
        this.mCallback = callback;
        callback.onStart();
        if (MyHttpClient.isNetworkConnected(mContext)) {
            requestToServer();
        } else {
            mNetworkError = new NetworkError(NetworkError.ERR_NO_INTERNET_CONNECTION);
            if (mCallback != null) mCallback.onCompleted(null, mNetworkError);
        }
    }

    protected abstract void requestToServer();

    public void removeCallback() {
        mCallback = null;
    }

    public static abstract class Callback<T> {
        protected void onStart() {}
        protected abstract void onCompleted(T result, NetworkError error);
    }
}
