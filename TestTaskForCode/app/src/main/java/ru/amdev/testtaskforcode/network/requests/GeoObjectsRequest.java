package ru.amdev.testtaskforcode.network.requests;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import ru.amdev.testtaskforcode.Constants;
import ru.amdev.testtaskforcode.interfaces.GeoObject;
import ru.amdev.testtaskforcode.network.MyHttpClient;
import ru.amdev.testtaskforcode.network.NetworkError;

public abstract class GeoObjectsRequest extends CacheableRequest<ArrayList<GeoObject>> {
    public static final String LOG_TAG = Constants.MY_LOGS + GeoObjectsRequest.class.getSimpleName();
    protected String url;
    protected RequestParams params;
    protected int count;
    protected int offset;

    public GeoObjectsRequest(Context context) {
        super(context);
    }

    public GeoObjectsRequest(Context context, int loadingPattern) {
        super(context, loadingPattern);
    }

    @Override
    protected void requestToServer() {
        initUrl();
        initRequestParams();
        Log.d(LOG_TAG, url + "?" + params.toString());
        MyHttpClient.getInstance().getClient().get(url, params, new JsonResponseHandler());
    }

    protected void initRequestParams() {
        params = new RequestParams();
        params.put("version", MyHttpClient.API_VERSION);
        if (count > 0) params.put("count", count);
        if (offset > 0) params.put("offset", offset);
    }

    protected abstract void initUrl();

    protected void parseAnswer(final JSONObject response) {
        new AsyncTask<Void, Void, ArrayList<GeoObject>>() {
            @Override
            protected ArrayList<GeoObject> doInBackground(Void... params) {
                try {
                    JSONArray jsonArray = response.getJSONArray("response");
                    ArrayList<GeoObject> geoObjects = (ArrayList<GeoObject>) getGeoObjects(jsonArray);
                    return geoObjects;
                } catch (JSONException e) {
                    mNetworkError = new NetworkError(NetworkError.ERR_OF_PARSING);
                    Log.e(LOG_TAG, "parseAnswer ex: " + e.toString());
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(ArrayList<GeoObject> geoObjects) {
                mCallback.onCompleted(geoObjects, mNetworkError);
            }
        }.execute();
    }

    protected abstract ArrayList<? extends GeoObject> getGeoObjects(JSONArray jsonArray);

    @Override
    protected void requestToCache() {

    }

    public GeoObjectsRequest offset(int offset) {
        this.offset = offset;
        return this;
    }

    public GeoObjectsRequest count(int count) {
        this.count = count;
        return this;
    }

    public class JsonResponseHandler extends JsonHttpResponseHandler {
        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
            Log.d(LOG_TAG, "onSuccess: " + response.toString());
            parseAnswer(response);
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
            Log.e(LOG_TAG, "onFailUre statusCode: " + statusCode);
            if (errorResponse != null)
                Log.e(LOG_TAG, "onFailUre response: " + errorResponse.toString());
            if (throwable != null) Log.e(LOG_TAG, throwable.toString());
            onRequestToServerFailure(new NetworkError(statusCode, errorResponse));
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            Log.e(LOG_TAG, "onFailUre statusCode:" + statusCode);
            if (responseString != null) Log.e(LOG_TAG, "stringRes: " + responseString);
            if (throwable != null) Log.e(LOG_TAG, throwable.toString());
            onRequestToServerFailure(new NetworkError(statusCode, null));
        }
    }

}
