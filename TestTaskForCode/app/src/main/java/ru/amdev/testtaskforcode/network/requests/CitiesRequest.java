package ru.amdev.testtaskforcode.network.requests;

import android.content.Context;

import org.json.JSONArray;

import java.util.ArrayList;

import ru.amdev.testtaskforcode.interfaces.GeoObject;
import ru.amdev.testtaskforcode.models.City;
import ru.amdev.testtaskforcode.network.MyHttpClient;

public class CitiesRequest extends GeoObjectsRequest {
    private int countryId;
    private int regionId;

    public CitiesRequest(Context context) {
        super(context);
    }

    public CitiesRequest(Context context, int loadingPattern) {
        super(context, loadingPattern);
    }

    @Override
    protected void initUrl() {
        url = MyHttpClient.API + "getCities";
    }

    @Override
    protected void initRequestParams() {
        super.initRequestParams();
        params.put("country_id", countryId);
        params.put("region_id", regionId);
    }

    @Override
    protected ArrayList<? extends GeoObject> getGeoObjects(JSONArray jsonArray) {
        return City.createFromJSONArray(jsonArray);
    }

    public CitiesRequest countryId(int countryId) {
        this.countryId = countryId;
        return this;
    }

    public CitiesRequest regionId(int regionId) {
        this.regionId = regionId;
        return this;
    }
}
