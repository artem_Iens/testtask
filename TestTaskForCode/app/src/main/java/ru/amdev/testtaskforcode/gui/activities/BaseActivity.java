package ru.amdev.testtaskforcode.gui.activities;

import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import ru.amdev.testtaskforcode.R;
import ru.amdev.testtaskforcode.network.NetworkError;

public abstract class BaseActivity extends AppCompatActivity {

    /**
     * if you use loaders and want to execute dialog in this method, you might get
     * "IllegalStateException: Can not perform this action inside of onLoadFinished"
     * Use Handler to avoid it.
     */
    public void handleNetworkError(final NetworkError error) {
        if (error == null) return;
        switch (error.code) {
            case NetworkError.ERR_NO_INTERNET_CONNECTION:
                showToast(getResources().getString(R.string.err_internet_connection_is_required));
                break;
            case NetworkError.ERR_SERVER_NO_RESPONSE:
                showToast(getResources().getString(R.string.err_request_failure));
                break;
            case NetworkError.ERR_4xx_FROM_SERVER:
                String errMsg = error.getErrMsg() == null ? getString(R.string.err_4xx_from_server) : error.getErrMsg();
                showToast(errMsg);
                break;
            case NetworkError.ERR_INTERNAL_SERVER:
                showToast(getResources().getString(R.string.err_internal_server));
                break;
            case NetworkError.ERR_OF_PARSING:
                showToast(getString(R.string.err_of_parsing));
                break;
            default:
                showToast(getResources().getString(R.string.err_undefined));
                break;
        }
    }

    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

}
