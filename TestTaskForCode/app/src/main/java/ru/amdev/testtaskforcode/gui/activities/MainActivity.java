package ru.amdev.testtaskforcode.gui.activities;

import android.content.Intent;
import android.os.Bundle;

import ru.amdev.testtaskforcode.R;
import ru.amdev.testtaskforcode.interfaces.GeoObject;
import ru.amdev.testtaskforcode.network.requests.CountriesRequest;
import ru.amdev.testtaskforcode.network.requests.GeoObjectsRequest;

public class MainActivity extends GeoObjectsActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(R.string.countries);
    }

    @Override
    protected GeoObjectsRequest createRequest(int offset) {
        GeoObjectsRequest request = new CountriesRequest(this)
                .count(COUNT_OF_MODELS_FOR_ONE_LOADING)
                .offset(offset);
        return request;
    }

    @Override
    public void onItemClick(GeoObject geoObject) {
        Intent intent = new Intent(this, RegionsActivity.class);
        intent.putExtra("country", geoObject);
        startActivity(intent);
    }

}
