package ru.amdev.testtaskforcode.network;

import org.json.JSONException;
import org.json.JSONObject;

public class NetworkError {
    public static final int ERR_UNDEFINED = 0;
    public static final int ERR_MSG_ONLY = 1;
    public static final int ERR_NO_INTERNET_CONNECTION = 2;
    public static final int ERR_SERVER_NO_RESPONSE = 3;
    public static final int ERR_OF_PARSING = 4;

    public static final int ERR_4xx_FROM_SERVER = 400;
    public static final int ERR_INTERNAL_SERVER = 500;
    public static final int ERR_UNAUTHORIZED = 401;

    public int code;

    protected String errMsg;

    public NetworkError(int code) {
        this.code = code;
    }

    public NetworkError(String errMsg) {
        this.errMsg = errMsg;
        this.code = ERR_MSG_ONLY;
    }

    public NetworkError(int statusCode, JSONObject errorResponse) {
        setErrorCode(statusCode);
        if(errorResponse != null) parseErrMsg(errorResponse);
    }

    public void setErrorCode(int statusCode) {
        if(statusCode == 401) {
            code = ERR_UNAUTHORIZED;
        } else if(statusCode >= 400 && statusCode < 500) {
            code = ERR_4xx_FROM_SERVER;
        } else if(statusCode == 0) {
            code = ERR_SERVER_NO_RESPONSE;
        } else if(statusCode == 500 || statusCode == 501) {
            code = ERR_INTERNAL_SERVER;
        }
    }

    public void parseErrMsg(JSONObject errorResponse) {
        try {
            errMsg = errorResponse.getString("message");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getErrMsg() {
        return errMsg;
    }
}
