package ru.amdev.testtaskforcode.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.loopj.android.http.AsyncHttpClient;

public class MyHttpClient {
    public static final String API = "https://api.vk.com/method/database.";
    public static final String API_VERSION = "5.45";

    private AsyncHttpClient client;
    private static volatile MyHttpClient instance;

    public static MyHttpClient getInstance() {
        MyHttpClient localInstance = instance;
        if (localInstance == null) {
            synchronized (MyHttpClient.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new MyHttpClient();
                }
            }
        }
        return localInstance;
    }

    private MyHttpClient() {
        client = new AsyncHttpClient();
    }

    public AsyncHttpClient getClient() {
        return client;
    }

    public static boolean isNetworkConnected(Context _context) {
        ConnectivityManager cm = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }
}
