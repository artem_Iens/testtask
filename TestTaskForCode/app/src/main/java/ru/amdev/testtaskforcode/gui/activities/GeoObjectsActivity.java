package ru.amdev.testtaskforcode.gui.activities;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;

import ru.amdev.testtaskforcode.R;
import ru.amdev.testtaskforcode.gui.adapters.GeoObjectsAdapter;
import ru.amdev.testtaskforcode.gui.items.GeoObjectItem;
import ru.amdev.testtaskforcode.interfaces.GeoObject;
import ru.amdev.testtaskforcode.loaders.BaseLoader;
import ru.amdev.testtaskforcode.loaders.RequestLoader;
import ru.amdev.testtaskforcode.network.requests.GeoObjectsRequest;

public abstract class GeoObjectsActivity  extends BaseActivity implements GeoObjectItem.ItemListener,
        LoaderManager.LoaderCallbacks<ArrayList<GeoObject>>, SwipeRefreshLayout.OnRefreshListener {
    public static final int GEO_OBJECTS_LOADER = 1;
    public static final int COUNT_OF_MODELS_FOR_ONE_LOADING = 100;

    protected SwipeRefreshLayout mSwipeLayout;
    protected LinearLayoutManager mLayoutManager;
    protected GeoObjectsAdapter mAdapter;
    protected ArrayList<GeoObject> mGeoObjects = new ArrayList<>();
    protected boolean mRefreshingData; //to clean up mGeoObjects list after success
    protected boolean mAllDataFetched;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        handleIntentExtras();
        initGUI();
        getSupportLoaderManager().initLoader(GEO_OBJECTS_LOADER, null, this);

        if (savedInstanceState == null) {
            ((RequestLoader) getSupportLoaderManager().getLoader(GEO_OBJECTS_LOADER))
                    .forceLoad(createRequest(0));
        } else {
            mRefreshingData = savedInstanceState.getBoolean("mRefreshingData", false);
            mAllDataFetched = savedInstanceState.getBoolean("mAllDataFetched", false);
            ArrayList<GeoObject> savedGeoObjects =
                    (ArrayList<GeoObject>) savedInstanceState.getSerializable("mGeoObjects");
            if (savedGeoObjects != null) mGeoObjects.addAll(savedGeoObjects);
            mAdapter.notifyDataSetChanged();
        }
    }

    protected void handleIntentExtras() {};

    protected void initGUI() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setPullToRefhresh(this);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recView);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new GeoObjectsAdapter(mGeoObjects, this);
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnScrollListener(new LoadMoreListener());
    }

    protected void setPullToRefhresh(SwipeRefreshLayout.OnRefreshListener listener) {
        mSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeLayout.setOnRefreshListener(listener);
        mSwipeLayout.setColorSchemeResources(R.color.pulltorefresh_first,
                R.color.pulltorefresh_second,
                R.color.pulltorefresh_first,
                R.color.pulltorefresh_second);
        mSwipeLayout.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
    }

    @Override
    protected void onStart() {
        super.onStart();
        showProgressIfNecessary();
    }

    protected void showProgressIfNecessary() {
        if (isLoaderWorks() && !mSwipeLayout.isRefreshing()) {
            mSwipeLayout.setRefreshing(true);
        }
    }

    protected boolean isLoaderWorks() {
        BaseLoader loader = (BaseLoader) getSupportLoaderManager().getLoader(GEO_OBJECTS_LOADER);
        return loader != null && loader.isWorking();
    }

    @Override
    public Loader<ArrayList<GeoObject>> onCreateLoader(int id, Bundle args) {
        return new RequestLoader<>(getApplicationContext());
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<GeoObject>> loader, ArrayList<GeoObject> data) {
        mSwipeLayout.setRefreshing(false);
        handleNetworkError(((RequestLoader) loader).getError());
        if (data != null) {
            if (mRefreshingData) mGeoObjects.clear();
            mAllDataFetched = data.size() < COUNT_OF_MODELS_FOR_ONE_LOADING;
            mGeoObjects.addAll(data);
            mAdapter.notifyDataSetChanged();
        }
        mRefreshingData = false;
        getSupportLoaderManager().destroyLoader(GEO_OBJECTS_LOADER);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<GeoObject>> loader) {
    }

    @Override
    public void onRefresh() {
        mRefreshingData = true;
        ((RequestLoader) getSupportLoaderManager()
                .restartLoader(GEO_OBJECTS_LOADER, null, this)).forceLoad(createRequest(0));
    }

    protected abstract GeoObjectsRequest createRequest(int offset);

    public class LoadMoreListener extends RecyclerView.OnScrollListener {
        private int pastVisiblesItems, visibleItemCount, totalItemCount;

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if (dy > 0 && !mAllDataFetched) {  //check for scroll down
                visibleItemCount = mLayoutManager.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                    if (!isLoaderWorks()) {
                        mSwipeLayout.setRefreshing(true);
                        //TODO show progress bottom item in recycler view
                        ((RequestLoader)getSupportLoaderManager()
                                .restartLoader(GEO_OBJECTS_LOADER, null, GeoObjectsActivity.this))
                                .forceLoad(createRequest(mGeoObjects.size()));
                    }
                }
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("mGeoObjects", mGeoObjects);
        outState.putBoolean("mAllDataFetched", mAllDataFetched);
        outState.putBoolean("mRefreshingData", mRefreshingData);
    }

}
