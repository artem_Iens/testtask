package ru.amdev.testtaskforcode.gui.activities;

import android.os.Bundle;

import ru.amdev.testtaskforcode.interfaces.GeoObject;
import ru.amdev.testtaskforcode.network.requests.CitiesRequest;
import ru.amdev.testtaskforcode.network.requests.GeoObjectsRequest;

public class CitiesActivity extends DetailGeoObjectsActivity {
    private GeoObject region;
    private GeoObject country;

    @Override
    protected void handleIntentExtras() {
        country = (GeoObject) getIntent().getSerializableExtra("country");
        region = (GeoObject) getIntent().getSerializableExtra("region");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(region.getTitle());
    }

    @Override
    protected GeoObjectsRequest createRequest(int offset) {
        CitiesRequest request = new CitiesRequest(this)
                .countryId(country.getId())
                .regionId(region.getId());
        request.count(COUNT_OF_MODELS_FOR_ONE_LOADING)
                .offset(offset);
        return request;
    }

    @Override
    public void onItemClick(GeoObject geoObject) {
        //TODO open CityMapActivity
    }
}
