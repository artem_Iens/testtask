package ru.amdev.testtaskforcode.network.requests;

import android.content.Context;

import org.json.JSONArray;

import java.util.ArrayList;

import ru.amdev.testtaskforcode.interfaces.GeoObject;
import ru.amdev.testtaskforcode.models.Region;
import ru.amdev.testtaskforcode.network.MyHttpClient;

public class RegionsRequest extends GeoObjectsRequest {
    private int countryId;

    public RegionsRequest(Context context) {
        super(context);
    }

    public RegionsRequest(Context context, int loadingPattern) {
        super(context, loadingPattern);
    }

    @Override
    protected void initUrl() {
        url = MyHttpClient.API + "getRegions";
    }

    @Override
    protected void initRequestParams() {
        super.initRequestParams();
        params.put("country_id", countryId);
    }

    public RegionsRequest countryId(int countryId) {
        this.countryId = countryId;
        return this;
    }

    @Override
    protected ArrayList<? extends GeoObject> getGeoObjects(JSONArray jsonArray) {
        return Region.createFromJSONArray(jsonArray);
    }
}
