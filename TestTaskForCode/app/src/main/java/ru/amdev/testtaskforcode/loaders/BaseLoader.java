package ru.amdev.testtaskforcode.loaders;

import android.content.Context;
import android.support.v4.content.Loader;

public abstract class BaseLoader<T> extends Loader<T> {
    protected boolean mWorking;
    protected T mData;
    protected boolean mWaitingForDelivery;

    public BaseLoader(Context context) {
        super(context);
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if (mWaitingForDelivery) {
            deliverResult(mData);
        }
    }

    @Override
    protected void onForceLoad() {
        mWorking = true;
    }

    @Override
    public void deliverResult(T data) {
        mWorking = false;
        if (isStarted()) {
            super.deliverResult(data);
        } else {
            mData = data;
            mWaitingForDelivery = true;
        }
    }

    public boolean isWorking() {
        return mWorking;
    }

}