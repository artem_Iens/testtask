package ru.amdev.testtaskforcode.models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.amdev.testtaskforcode.Constants;
import ru.amdev.testtaskforcode.interfaces.GeoObject;

public class Region implements GeoObject {
    public static final String LOG_TAG = Constants.MY_LOGS + Region.class.getSimpleName();
    public int id;
    public String title;

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public int getId() {
        return id;
    }

    public static ArrayList<Region> createFromJSONArray(JSONArray jsonArray) {
        ArrayList<Region> models = new ArrayList<>();
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                Region model = createFromJSON(jsonArray.getJSONObject(i));
                if (model != null) models.add(model);
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "createFromJSONArray ex: " + e.toString());
            e.printStackTrace();
        }
        return models;
    }

    public static Region createFromJSON(JSONObject jsonObject) {
        Region model = new Region();
        try {
            model.id = Integer.valueOf(jsonObject.getString("region_id"));
            model.title = jsonObject.getString("title");
        } catch (JSONException e) {
            Log.e(LOG_TAG, "createFromJSON ex: " + e.toString());
            e.printStackTrace();
            return null;
        }
        return model;
    }
}
