package ru.amdev.testtaskforcode.network.requests;

import android.content.Context;

import org.json.JSONArray;

import java.util.ArrayList;

import ru.amdev.testtaskforcode.interfaces.GeoObject;
import ru.amdev.testtaskforcode.models.Country;
import ru.amdev.testtaskforcode.network.MyHttpClient;

public class CountriesRequest extends GeoObjectsRequest {

    public CountriesRequest(Context context) {
        super(context);
    }

    public CountriesRequest(Context context, int loadingPattern) {
        super(context, loadingPattern);
    }

    @Override
    protected void initUrl() {
        url = MyHttpClient.API + "getCountries";
    }

    @Override
    protected void initRequestParams() {
        super.initRequestParams();
        params.put("need_all", 1);
    }

    @Override
    protected ArrayList<? extends GeoObject> getGeoObjects(JSONArray jsonArray) {
        return Country.createFromJSONArray(jsonArray);
    }
}
