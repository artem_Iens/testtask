package ru.amdev.testtaskforcode.gui.items;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import ru.amdev.testtaskforcode.R;
import ru.amdev.testtaskforcode.interfaces.GeoObject;

public class GeoObjectItem extends RecyclerView.ViewHolder {
    private GeoObject geoObject;
    private ItemListener listener;
    private TextView tvTitle;

    public GeoObjectItem(View itemView, final ItemListener listener) {
        super(itemView);
        this.listener = listener;
        tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onItemClick(geoObject);
            }
        });
    }

    public void setData(GeoObject geoObject) {
        this.geoObject = geoObject;
        tvTitle.setText(geoObject.getTitle());
    }

    public interface ItemListener {
        public void onItemClick(GeoObject geoObject);
    }
}