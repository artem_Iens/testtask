package ru.amdev.testtaskforcode.gui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ru.amdev.testtaskforcode.R;
import ru.amdev.testtaskforcode.gui.items.GeoObjectItem;
import ru.amdev.testtaskforcode.interfaces.GeoObject;

public class GeoObjectsAdapter extends RecyclerView.Adapter<GeoObjectItem> {
    protected GeoObjectItem.ItemListener mListener;
    protected List<GeoObject> mData;

    public GeoObjectsAdapter(List<GeoObject> data, GeoObjectItem.ItemListener listener) {
        this.mData = data;
        this.mListener = listener;
    }

    @Override
    public GeoObjectItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_geo_object, parent, false);
        GeoObjectItem item = new GeoObjectItem(view, mListener);
        return item;
    }

    @Override
    public void onBindViewHolder(GeoObjectItem holder, int position) {
        holder.setData(mData.get(position));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
