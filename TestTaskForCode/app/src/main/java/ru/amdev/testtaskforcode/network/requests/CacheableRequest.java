package ru.amdev.testtaskforcode.network.requests;

import android.content.Context;

import ru.amdev.testtaskforcode.network.MyHttpClient;
import ru.amdev.testtaskforcode.network.NetworkError;

public abstract class CacheableRequest<T> extends Request<T> {
    public static final int LOADING_WITHOUT_CACHE = 0;
    public static final int LOADING_CACHE_ON_SERVER_FAILURE = 1;
    public static final int FORCE_LOADING_CACHE = 2;
    protected int mLoadingPattern;

    //Do not pass Activity or Fragment to constructors, it may be cause of memory leaks
    //Use Application context instead
    public CacheableRequest(Context context) {
        super(context);
    }

    public CacheableRequest(Context context, int loadingPattern) {
        super(context);
        this.mLoadingPattern = loadingPattern;
    }

    public void execute(Callback<T> callback) {
        this.mCallback = callback;
        callback.onStart();
        if (mLoadingPattern == FORCE_LOADING_CACHE) {
            requestToCache();
        } else {
            getDataFromServer();
        }
    }

    protected void getDataFromServer() {
        if (MyHttpClient.isNetworkConnected(mContext)) {
            requestToServer();
        } else {
            mNetworkError = new NetworkError(NetworkError.ERR_NO_INTERNET_CONNECTION);
            if (mLoadingPattern == LOADING_WITHOUT_CACHE) {
                if (mCallback != null) mCallback.onCompleted(null, mNetworkError);
            } else {
                requestToCache();
            }
        }
    }

    protected abstract void requestToCache();

    protected void onRequestToServerFailure(NetworkError networkError) {
        if (mCallback == null) return;
        this.mNetworkError = networkError;
        if(mLoadingPattern == LOADING_CACHE_ON_SERVER_FAILURE) {
            requestToCache();
        } else {
            mCallback.onCompleted(null, networkError);
        }
    }

    //no data in cache
    protected void onRequestToCacheFailure() {
        if (mCallback == null) return;
        if (mLoadingPattern == FORCE_LOADING_CACHE) {
            requestToServer();
        } else {
            mCallback.onCompleted(null, mNetworkError);
        }
    }

}
